<?php
	session_start();

	$connection = @mysqli_connect('192.168.20.75', 'patrick', 'qwerty', 'abs');

	if (!$connection) {
    	die( 'Connect Error: ' . mysqli_connect_errno() );
	}

	if ( isset($_REQUEST["action"]) )
	{
		if ( $_REQUEST["action"] == "search")
		{
			search($connection, $_REQUEST["jobNumber"]);

		} else if ( $_REQUEST["action"] == "trackJob") {
			trackJob($connection, $_REQUEST["jobNumber"], $_SESSION["username"]);

		} else if ( $_REQUEST["action"] == "fetchAllJobs" ) {
			fetchAllJobs($connection, $_REQUEST["orderBy"], $_REQUEST["order"]);

		} else if ( $_REQUEST["action"] == "fetchJobsForUser" ) {
			fetchJobsForUser($connection, $_REQUEST["username"],  $_REQUEST["orderBy"], $_REQUEST["order"]);
		}
	}

	mysqli_close($connection);

	function search($connection, $jobNumber)
	{
		$result = array();

		if ( strlen( $jobNumber ) > 0 )
		{	
			// Fetch everything
			$sql = "	SELECT * FROM jobs
				WHERE jobNumber LIKE '" . $jobNumber ."%';
			";

			$query = mysqli_query($connection, $sql);
			
			// Bail if query fails.
			if ( $query )
			{			
				while($row = mysqli_fetch_array($query))
				{
					$rowArray["jobNumber"] = $row["jobNumber"];
					$rowArray["username"] = $row["username"];
					$rowArray["date"] = $row["date"];

					array_push($result, $rowArray);
				}
			}
		}

		echo json_encode($result);
	}

	function trackJob($connection, $jobNumber, $username)
	{
		//$sql = 'INSERT INTO jobs (jobNumber, username) VALUES("' . $_POST["jobNumber"] . '", "' . 'test' . '")';
		$sql = 'INSERT INTO jobs (jobNumber, username) VALUES("' . $_POST["jobNumber"] . '", "' . $username . '")';

		if ( mysqli_query($connection, $sql) )
		{
			$insert_id = mysqli_insert_id($connection);	// id used in last query
			$query = mysqli_query($connection, 'SELECT * FROM jobs WHERE jobs.id = ' . $insert_id);

			if ( $query )
			{
				$result = array();

				while ( $row = mysqli_fetch_array($query) )
				{
					$rowArray["jobNumber"] = $row["jobNumber"];
					$rowArray["username"] = $row["username"];
					$rowArray["date"] = $row["date"];

					array_push($result, $rowArray);
				}

				echo json_encode($result);

			} else {
				echo 'There was an error.';
			}

		} else {
			echo 'There was an error.';
		}
	}

	function fetchAllJobs( $connection, $orderBy, $order )
	{
		$query = mysqli_query($connection, "SELECT * FROM jobs ORDER BY " . $orderBy . " " . $order );

		if ( $query )
		{
			$result = array();

			while ( $row = mysqli_fetch_array($query) )
			{
				$rowArray["jobNumber"] = $row["jobNumber"];
				$rowArray["username"] = $row["username"];
				$rowArray["date"] = $row["date"];

				array_push($result, $rowArray);
			}

			echo json_encode($result);
		}
	}

	function fetchJobsForUser($connection, $username, $orderBy, $order )
	{
		$query = mysqli_query($connection, "SELECT * FROM jobs WHERE username = '" . $username . "' ORDER BY " . $orderBy . " " . $order );

		if ( $query )
		{
			$result = array();

			while ( $row = mysqli_fetch_array($query) )
			{
				$rowArray["jobNumber"] = $row["jobNumber"];
				$rowArray["username"] = $row["username"];
				$rowArray["date"] = $row["date"];

				array_push($result, $rowArray);
			}

			echo json_encode($result);
		} else {
			// no results found if query is null
			echo $username;
		}
	}
?>