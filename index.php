<?php
	// Check if user is logged in already.

	// Inialize session
	session_start();

	// Check, if username session is NOT set then this page will jump to login page
	if ( !isset( $_SESSION['username'] ) )
	{
		header('Location: login.html');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<!-- 	http://stackoverflow.com/questions/22777854/barcode-scanner-for-html5-and-jquery-application
		http://stackoverflow.com/questions/22777854/barcode-scanner-for-html5-and-jquery-application
		https://code.google.com/p/jquery-barcodelistener/wiki/setup
		http://www.carolinabarcode.com/barcode-font-code-39-full-ascii-scanner-a-75.html
		http://stackoverflow.com/questions/1898129/javascript-subtract-keycode
		http://stackoverflow.com/questions/291813/recommended-way-to-embed-pdf-in-html
		http://stackoverflow.com/questions/20391437/e-which-keycode-for-minus-hyphen-has-changed -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tracker</title>

	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

	<!--<script src="jquery.barcodelistener-1.1-min.js"></script>-->

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>

	<!--<link href="css/style.css" rel="stylesheet">-->

	<script>
		// ENUMS
		TableSortEnum = {
			DATE_DESC : 0,
			DATE_ASC : 1,
			USERNAME_ASC : 2,
			USERNAME_DESC : 3
		}

		// Get current user's name.
		var USERNAME = "<?php echo $_SESSION['username'] ?>";
		var barcodeScanningMode = false;
		var barcode = "";

		$(document).ready(function() {
			// Show current user's name.
			$('#currentUser').text(capitalize(USERNAME));

			/*
			char0 = new Array("�", "32");
			char1 = new Array("�", "732");
			characters = new Array(char0, char1);

			$(document).BarcodeListener(characters, function(code) {
         				// YOUR FUNCTION HERE
				// for example:
				alert(code);
			});
			*/

			// ===============================================================
			// ====================== EVENT LISTENERS ========================
			// ===============================================================			
			

			$(document).keydown(function(e) {
				if ( barcodeScanningMode )
				{
					var code = (e.keyCode ? e.keyCode : e.which);

					if(code==13)// Enter key hit
					{
						trackJob(barcode);
						barcode = "";
					}
					else if(code==9)// Tab key hit
					{

	 				}
					else
					{
						if ( code == 189 )
						{
							barcode = barcode + "-";
						} else {
							var c = String.fromCharCode(code);
							if ( c.match(/[\w|-]/i) )	// only alphanumeric characters (case-insensitive) and hiphens allowed.
							{
								barcode = barcode + c;
							}
						}
	 				}
 				}
			});

			/*
			// temporary fix to distinguish between keyboard and barcode scanner.
			$('h1').click(function(){
				console.log("barcode cleared.");
				barcode = "";
			});*/

			// Event triggerd when a tab is show.
			$('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
				//console.log(e.target); // new tab
				//console.log(e.relatedTarget);	// previous tab

				var target = $(e.target).attr('href');

				switch(target)
				{
					case "#myJobs":
						fetchJobsForUser(USERNAME);
						break;
					case "#allJobs": 
						fetchAllJobs();
						break;
				}
			});

			$('#barcodeButton').click(function(event) {
				var btn = $(this);

				if ( !barcodeScanningMode )
				{
					// Stop dcouemnt click event from triggering.
					event.stopPropagation();

					barcodeScanningMode = true;
					barcode = "";

					// Change button text.
					btn.button('loading');
				}
			});

			$(document).click(function(event) {
				if ( barcodeScanningMode )
				{
					barcodeScanningMode = false;

					$('#barcodeButton').button('reset');
				}
			});
		});

		// ===============================================================
		// ========================= FUNCTIONS ===========================
		// ===============================================================

		function capitalize(string)
		{
			return (string.charAt(0).toUpperCase() + string.toLowerCase().slice(1));
		}

		function trackButtonPressed()
		{
			var jobNumber = $('#jobNumberInput').val().trim().toUpperCase();

			if ( jobNumber == null || jobNumber == "" )
			{
				alert("Enter a job number or use barcode tracking.");
			} else {
				trackJob(jobNumber);
			}

		}

		function trim(string)
		{
			return string.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		}

		function validateJobNumber( string )
		{
			return string.match(/^(\w{4})(-\w)?$/);
		}

		function search( jobNumber )
		{
			if ( jobNumber == null || jobNumber == "" )
			{
				$('#search-result').empty();
			} else {
				buildTable("action=search&jobNumber=" + jobNumber, "GET", $('#search-result'), false);
			}
		}

		function trackJob( jobNumber )
		{
			if ( validateJobNumber(jobNumber) )
			{
				console.log("Tracking: " + jobNumber);
				buildTable("action=trackJob&jobNumber=" + jobNumber, "POST", $('#track-result'), true);
			} else {
				alert(jobNumber + " is an invalid job number.");
			}
		}

		function fetchAllJobs()
		{
			buildTable("action=fetchAllJobs&" + getTableSort(), "POST", $('#all-jobs-result'), false);
		}

		function fetchJobsForUser(username)
		{
			buildTable("action=fetchJobsForUser&username=" + username + "&" + getTableSort(), "POST", $('#my-jobs-result'), false);
		}

		function fetchJobsForCurrentUser()
		{
			fetchJobsForUser(USERNAME);
		}

		/**

		*/
		function getTableSort()
		{
			// Get sorting order from <select> element.
			var sortSelectedIndex = $('.tab-pane.active').find('#sortTableBy').prop("selectedIndex");

			switch ( sortSelectedIndex )
			{
				case TableSortEnum.DATE_DESC :
					return "orderBy=date&order=desc";
				case TableSortEnum.DATE_ASC :
					return "orderBy=date&order=asc";
				case TableSortEnum.USERNAME_ASC :
					return "orderBy=username&order=asc";
				case TableSortEnum.USERNAME_DESC :
					return "orderBy=username&order=desc";
				default:
					return "orderBy=date&order=desc";
			}
		}

		/**

		*/
		function clearTableButtonPressed()
		{
			// Find the table in the active tab and delete all table rows <tr> except the first (the headers<td>).
			$('.tab-pane, .active').find('table tr').each(function(i){
				if ( i > 0 )
				{
					$(this).remove();
				}
			});
		}

		/**
			Fetches data from the server. Generates a table with the results.
			@param sendData
				The request data to send to the server. Example String, "action=trackJobForUser&username=myname".
				See actions.php for list of actions/functions.
			@param table
				The jquery table element to insert the data into.
			@param append
				Boolean. TRUE to append data to an existing table. False start a new table.
		*/
		function buildTable(sendData, type, table, append)
		{
			if ( sendData != null && table != null && append != null)
			{
				$.ajax({
					url: "php/actions.php",
					type: type,
					data: sendData,
					dataType: "JSON",
					success: function(data) {
						// DEBUG
						//if ( data ) console.log(data); else console.log("data is null");
						//return;
						if ( data != null )
						{
							if (!append)
							{
								table.html("<tr><th>Job#</th><th>User</th><th>Date</th></tr>");
							}

							for ( var i in data )
							{
								// Highlight rows that 'belong' to the current user.
								if ( data[i].username.toLowerCase() == USERNAME.toLowerCase() )
								{
									table.append(	"<tr class='info'>" +
													"<td>" + data[i].jobNumber + "</td>" +
													"<td>" + capitalize(data[i].username) + "</td>" +
													"<td>" + data[i].date + "</td>" +
													"</tr>" 
												);
								} else {
									table.append(	"<tr>" +
													"<td>" + data[i].jobNumber + "</td>" +
													"<td>" + capitalize(data[i].username) + "</td>" +
													"<td>" + data[i].date + "</td>" +
													"</tr>" 
												);									
								}
							}
						}
						
					},
					error: function(xhr, status, error) {
						alert("There was an error fetching data from the server.");
						if ( error )
							console.log("There was an error fetching data from the server.\n" + error + "\nsendData: " + sendData);
					}
				});
			}
		}

	</script>
	<style>

	/* Adds top-margin to rows. */
	.spacer-1
	{
		margin-top: 1em;
	}
	</style>
</head>

<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-right">
					<span id="currentUser"></span>
					<a href="php/logout.php">Logout</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center">Tracking</h1>
					<ul id="nav-tabs" class="nav nav-tabs" role="tablist">
						<li class="active"><a href="#search" role="tab" data-toggle="tab">Search</a></li>
						<li><a href="#track" role="tab" data-toggle="tab">Track</a></li>
						<li><a href="#myJobs" role="tab" data-toggle="tab">My Jobs</a></li>
						<li><a href="#allJobs" role="tab" data-toggle="tab">All Jobs</a></li>
					</ul>
					<div class="tab-content">
						<!-- -------------------------------- SEARCH ----------------------------------------- -->
						<div class="tab-pane active" id="search">
							<div class="row spacer-1">
								<div class="col-md-3">
									<form action="javascript:void(0)" role="form">
										<div class="form-group">
											<label for="searchJobNumber">Job #</label>
											<input type="text" class="form-control" placeholder="Job #" onkeyup="search(this.value)">
										</div>
										<p>Options</p>
										<div class="form-group">
											<label for="sortTableBy">Sort by:</label>
											<select class="form-control" id="sortTableBy" onChange="search()">
												<option>Date (newest first)</option>
												<option>Date (oldest first)</option>
											</select>
										</div>
									</form>
								</div>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="search-result">

									</table>
								</div>
							</div>
						</div>
						<!-- -------------------------------- TRACK ----------------------------------------- -->
						<div class="tab-pane" id="track">
							<div class="row spacer-1">
								<div class="col-md-3">
									<form id="trackControls" action="javascript:void(0)" role="form">
										<div class="form-group">
											<label for="searchJobNumber">Job #</label>
											<input id="jobNumberInput" type="text" class="form-control" placeholder="Job #">
										</div>
										<div class="form-group">
											<button type="button" class="btn btn-primary" onClick="trackButtonPressed()">Track</button>
										</div>
										<div class="form-group">
											<button id="barcodeButton" type="button" class="btn btn-primary" data-loading-text="Stop">Start Barcode Scanning</button>
										</div>
									</form>
								</div>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="track-result">
										<tr><th>Job#</th><th>User</th><th>Date</th></tr>

									</table>
									<div class="row">
										<div class="col-md-9">
											<button type="button" class="btn btn-primary" onClick="clearTableButtonPressed()">Clear</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- -------------------------------- MY JOBS ---------------------------------------- -->
						<div class="tab-pane" id="myJobs">
							<div class="row spacer-1">
								
								<div class="col-md-3">
									<form role="form">
										<div class="form-group">
											<label for="sortTableBy">Sort by:</label>
											<select class="form-control" id="sortTableBy" onChange="fetchJobsForCurrentUser()">
												<option>Date (newest first)</option>
												<option>Date (oldest first)</option>
											</select>
										</div>
									</form>
								</div>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="my-jobs-result">

									</table>
								</div>
							</div>
						</div>
						<!-- --------------------------------- ALL JOBS -------------------------------------- -->
						<div class="tab-pane" id="allJobs">
							<div class="row spacer-1">
								<div class="col-md-3">
									<form role="form">
										<div class="form-group">
											<label for="sortTableBy">Sort by:</label>
											<select class="form-control" id="sortTableBy" onChange="fetchAllJobs()">
												<option>Date (newest first)</option>
												<option>Date (oldest first)</option>
												<option>User</option>
											</select>
										</div>
									</form>
								</div>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="all-jobs-result">
										<tr><th>Job#</th><th>User</th><th>Date</th></tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--<div class="embed-responsive embed-responsive-16by9">
		<embed src="pdf.pdf" type="application/pdf" width="500" height="375"></embed>
		<iframe src="pdf.pdf" style='border:0'></iframe>
	</div>-->
</body>
</html>