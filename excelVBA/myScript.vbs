' The following is code to automate this process with task scheduler
' see: http://www.mrexcel.com/forum/excel-questions/302970-task-scheduler-vbulletin-script-auto-open-excel.html
' to execute: cscript myScript.vbs "C:\xampp\htdocs\track\excelVBA\dupa.xlsm"

Dim args, objExcel

Set args = WScript.Arguments
Set objExcel = CreateObject("Excel.Application")

objExcel.Workbooks.Open args(0)
objExcel.Visible = True

objExcel.Run "dupa.InsertData"

objExcel.ActiveWorkbook.Save
objExcel.ActiveWorkbook.Close (0)
objExcel.Quit
